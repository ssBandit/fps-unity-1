﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{
    public float speed = 5;
    public float sprintMultiplyer = 2;

    public float jumpForce = 10;

    float originalSpeed;

    public Weapon currentWeapon;

    Rigidbody rb;
    Camera cam;

    float yRot = 0;
    float xRot = 0;

    bool isGrounded = true;

    void Start()
    {
        originalSpeed = speed;
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        float mousex = Input.GetAxis("Mouse X");
        float mousey = Input.GetAxis("Mouse Y");

        isGrounded = Physics.Raycast(transform.position, Vector3.down, 1.2f);

        //Sprinting
        if(Input.GetButtonDown("Sprint")) speed *= sprintMultiplyer;
        if(Input.GetButtonUp("Sprint")) speed = originalSpeed;

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        Debug.DrawRay(cam.transform.position, cam.transform.forward * 100, Color.green);
        if(Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            Physics.Raycast(cam.transform.position, cam.transform.forward, out hit);
            
            currentWeapon.Fire(hit);
        }

        yRot += mousex;
        xRot += mousey;

        Vector3 moveDir = transform.right * h + transform.forward * v;

        rb.velocity = new Vector3(0, rb.velocity.y, 0) + moveDir * speed;

        xRot = Mathf.Clamp(xRot, -90, 90);

        transform.rotation = Quaternion.Euler(0, yRot, 0);
        cam.transform.localRotation = Quaternion.Euler(-xRot, 0, 0);
    }
}
