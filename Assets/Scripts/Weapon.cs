﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public Transform shootPoint;
    public LineRenderer line;
    public int maxAmmo = 10;
    public int currentAmmo = 10;
    public int damage = 1;
    public float shootForce = 10;
    public float scatter = 0;

    public virtual void Fire(RaycastHit hit)
    {
        if (currentAmmo <= 0) return;
        //currentAmmo--;

        line.enabled = true;

        line.SetPosition(0, shootPoint.position);

        if (hit.transform != null)
        {
            line.SetPosition(1, hit.point);

            //Check if hit enemy
            Enemy enemy = hit.transform.GetComponent<Enemy>();
            if(enemy != null)
            {
                enemy.Hit(damage);
            }

            //Check if hit rigidbody
            Rigidbody rb = hit.transform.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.AddExplosionForce(shootForce, hit.point, 1, 1, ForceMode.VelocityChange);
            }
        }
        else
        {
            line.SetPosition(1, transform.parent.forward * 99999999);
            Debug.Log(transform.parent.name);
        }

        StartCoroutine(HideLine());
    }

    public virtual void Reload()
    {
        currentAmmo = maxAmmo;
    }

    IEnumerator HideLine()
    {
        yield return new WaitForSeconds(0.02f);
        line.enabled = false;
    }
}
