﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon
{
    public override void Fire(RaycastHit hit)
    {
        base.Fire(hit);
        Debug.Log("pistol fire");
    }

    public override void Reload()
    {
        base.Reload();
    }
}
