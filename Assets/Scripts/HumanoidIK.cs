﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidIK : MonoBehaviour
{
    public Transform target;
    public Transform elbow;

    public Transform head;
    public Transform headTarget;

    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void LateUpdate()
    {
        if (Vector3.Angle(head.forward, headTarget.position) < 90)
        {
            head.LookAt(headTarget);
        }
    }

    void OnAnimatorIK(int layerIndex)
    {
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
        anim.SetIKPosition(AvatarIKGoal.RightHand, target.position);

        anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
        anim.SetIKRotation(AvatarIKGoal.RightHand, target.rotation * Quaternion.Euler(0,0,-90));

        anim.SetIKHintPositionWeight(AvatarIKHint.RightElbow, 1);
        anim.SetIKHintPosition(AvatarIKHint.RightElbow, elbow.position);
    }
}
