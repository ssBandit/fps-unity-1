using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueStarter : MonoBehaviour
{
    public GameObject textBox;
    public Text dialogueText;

    private void Start()
    {
        textBox.SetActive(false);
    }

    void Update()
    {
        if(Input.GetButtonDown("Use"))
        {
            Collider[] objects = Physics.OverlapSphere(transform.position, 1);

            foreach (Collider item in objects)
            {
                Dialogue dialogue = item.transform.GetComponent<Dialogue>();
                if(dialogue != null)
                {
                    if(dialogue.isLast)
                    {
                        textBox.SetActive(false);
                        dialogue.isLast = false;
                        Time.timeScale = 1;
                        break;
                    }
                    Time.timeScale = 0;
                    textBox.SetActive(true);
                    dialogueText.text = dialogue.AdvanceDialogue();
                    break;
                }
            }
        }
    }
}
