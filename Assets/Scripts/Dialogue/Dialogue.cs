using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogue : MonoBehaviour
{
    [TextArea]
    public string[] dialogue;
    int currentLine = 0;
    [HideInInspector]
    public bool isLast = false;

    public string AdvanceDialogue()
    {
        string text = dialogue[currentLine];

        isLast = currentLine == dialogue.Length - 1;

        currentLine++;
        currentLine = currentLine % dialogue.Length;

        return text;
    }
}
