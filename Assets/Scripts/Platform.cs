using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public float platformForce = 5;

    private void OnCollisionEnter(Collision collision)
    {
        Rigidbody rb = collision.transform.GetComponent<Rigidbody>();
        if(rb != null)
        {
            rb.AddForce(transform.up * platformForce, ForceMode.VelocityChange);
        }
    }
}
