using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public int health = 10;
    public Image healthbar;

    NavMeshAgent agent;
    Navigator navigation;
    Collider col;
    Animator anim;
    HumanoidIK ik;

    Collider[] colliders;
    Rigidbody[] rigidbodies;
    
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        navigation = GetComponent<Navigator>();
        col = GetComponent<Collider>();
        anim = GetComponent<Animator>();
        ik = GetComponent<HumanoidIK>();

        colliders = GetComponentsInChildren<Collider>();
        rigidbodies = GetComponentsInChildren<Rigidbody>();

        foreach(Collider col in colliders)
        {
            col.enabled = false;
        }
        foreach(Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = true;
        }

        colliders[0].enabled = true;

    }
    
    public void Hit(int damage)
    {
        health -= damage;

        healthbar.fillAmount = health / 10.0f;

        if(health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        navigation.enabled = false;
        agent.enabled = false;
        col.enabled = false;
        anim.enabled = false;
        ik.enabled = false;
        healthbar.transform.parent.gameObject.SetActive(false);

        //Enable ragdoll
        foreach (Collider col in colliders)
        {
            col.enabled = true;
        }
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = false;
        }

        colliders[0].enabled = false;
    }
}
